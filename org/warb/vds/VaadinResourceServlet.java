package org.warb.vds;

import javax.servlet.annotation.WebServlet;

import com.vaadin.server.VaadinServlet;

@WebServlet(value = "/VAADIN/*",
	asyncSupported = true)
public class VaadinResourceServlet extends VaadinServlet {
	// This servlet does nothing, it is only here to provide a servlet
	// for the Vaadin resources in /VAADIN/.
}
