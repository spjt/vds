package org.warb.vds;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServletRequest;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Title("Dispatch UI")
@Theme("valo")
public class ViewDispatchUI extends UI {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ViewDispatchUI.class);

    Navigator navigator;
    VaadinRequest vaadinRequest;

    // This is used so any exceptions thrown in a view will be displayed as specified in createErrorPage
    @Override
    public void doInit(VaadinRequest request, int uiId, String embedId) {
        this.setErrorHandler(new DefaultErrorHandler() {
            @Override
            public void error(com.vaadin.server.ErrorEvent errorEvent) {
                log.error("Error");
            }
        });
        super.doInit(request, uiId, embedId);
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        try {
            this.vaadinRequest = vaadinRequest;
            View v = ViewMap.getView(this, vaadinRequest.getPathInfo());
            navigator = new Navigator(this, this);
            navigator.addView("", v);
        } catch (Throwable t) {
            createErrorPage(t);
        }
    }

    @Override
    public Navigator getNavigator() {
        return navigator;
    }

    @Override
    public void detach() {
        log.info("detaching " + vaadinRequest.toString());
        super.detach();
    }

    public VaadinRequest getVaadinRequest() {
        return vaadinRequest;
    }

    public VaadinServletRequest getVaadinServletRequest() {
        return (VaadinServletRequest) vaadinRequest;
    }

    protected void createErrorPage(Throwable t) {
        String content = "<b>An error has occurred:</b><br/><br/>";

        // Display the error message in a custom fashion
        VerticalLayout layout = new VerticalLayout();
        content += getErrorMessage(t);
        layout.addComponent(new Label(content, ContentMode.HTML));
        UI.getCurrent().setContent(layout);
    }

    protected String getErrorMessage(Throwable t) {
        StringBuilder message = new StringBuilder();
        String myClassRegex = "(org.warb.vds|other.packages).*";
        Throwable endCause = null;
        for (; t != null; t = t.getCause()) {
            if (t.getCause() == null) { // We're at final cause
                endCause = t;
            }
        }
        if (endCause == null) {
            // should never happen
            throw new RuntimeException(t);
        }

        message.append(endCause.getClass().getCanonicalName()).
                append(" : ").
                append(endCause.getMessage()).
                append("<br/><br/>");

        StackTraceElement[] stackTraceElements = endCause.getStackTrace();
        for (StackTraceElement ste : stackTraceElements) {
            if (ste.getClassName().matches(myClassRegex)) {
                message.append(ste.getClassName()).
                        append(".").
                        append(ste.getMethodName()).
                        append(":").
                        append(ste.getLineNumber()).
                        append("<br/>");

            }
        }
        return message.toString();
    }
}
