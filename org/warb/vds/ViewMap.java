package org.warb.vds;

import java.util.HashMap;
import java.util.Set;

import org.reflections.Reflections;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import com.vaadin.navigator.View;

/**
 * This creates a map of annotated views to their classes.
 */
public class ViewMap {

    private static HashMap<String, Class<? extends View>> viewMap;
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ViewMap.class);
    private static final String searchPath;

    static {
        searchPath = ViewMap.class.getPackage().getName();
        refresh();
    }

    /**
     * Get the Class for a view name.
     *
     * @param viewName The name of the view
     * @return The Class for the view.
     */
    public static Class getViewClass(String viewName) {
        if (viewName.length() > 1 && viewName.charAt(0) == '/') {
            viewName = viewName.split("/")[1];
        }
        return viewMap.get(viewName);
    }

    /**
     * Get the View for a view.
     *
     * @param dispatchUI The ViewDispatchUI that will be calling the View.
     * @param viewName The name of the view
     * @return the named View.
     */
    public static View getView(ViewDispatchUI dispatchUI, String viewName) {
        if (viewName.length() > 1 && viewName.charAt(0) == '/') {
            viewName = viewName.split("/")[1];
        }
        Class<? extends View> viewClass = viewMap.get(viewName);
        log.info("Dispatching view class: " + viewClass.getCanonicalName());
        try {
            return viewClass.getConstructor(ViewDispatchUI.class).newInstance(dispatchUI);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Refresh the map if new Views have been added. In production this would only be
     * called once when this class is first invoked. For development, this can be called
     * at any time to refresh the map.
     */
    public static void refresh() {
        viewMap = new HashMap<>();
        org.reflections.Configuration configuration =
                new ConfigurationBuilder().setUrls(ClasspathHelper.forPackage(searchPath)).
                        setScanners(new TypeAnnotationsScanner(), new SubTypesScanner());
        Reflections ref = new Reflections(configuration);

        Set<Class<?>> foundClasses = ref.getTypesAnnotatedWith(VDSView.class);
        for (Class<?> implClass : foundClasses) {
            VDSView st = implClass.getAnnotation(VDSView.class);
            String name = st.name();
            log.info("Adding dispatched view [" + name + "]");
            viewMap.put(st.name(), (Class<? extends View>) implClass);
        }
    }
}
