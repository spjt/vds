package org.warb.vds.views;

/**
 * Created by phil on 7/8/15.
 */

import com.vaadin.ui.*;
import org.warb.vds.VDSView;
import org.warb.vds.ViewDispatchUI;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;

@VDSView(name = "google")
public class GoogleView extends VerticalLayout implements View {
	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(GoogleView.class);
	public GoogleView(ViewDispatchUI ui) {
		// Have a clickable button
		TextField tf = new TextField("Search for");
		addComponent(tf);
		addComponent(new Button("Go", e -> {
			ui.getPage().setLocation("https://google.com/search?q="+tf.getValue());
		}));
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
	}
}
