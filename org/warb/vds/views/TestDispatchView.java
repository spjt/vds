package org.warb.vds.views;

/**
 * Created by phil on 7/8/15.
 */

import org.warb.vds.VDSView;
import org.warb.vds.ViewDispatchUI;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

@VDSView(name = "dispatch")
public class TestDispatchView extends VerticalLayout implements View {

	public TestDispatchView(ViewDispatchUI ui) {

		// Display the greeting
		addComponent(new Label("This view was launched by reflective dispatch."));

		// Have a clickable button
		addComponent(new Button("Push Me!", e -> {
			Notification.show("Pushed!");
		}));

	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

	}
}
