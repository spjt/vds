package org.warb.vds.views;

import java.util.Arrays;

import org.warb.vds.VDSView;
import org.warb.vds.ViewDispatchUI;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.VerticalLayout;

/**
 * Created by phil on 7/8/15.
 */
@VDSView(name="themes")
public class ChangeThemeView extends VerticalLayout implements View {

	public ChangeThemeView(ViewDispatchUI vdi) {
		String[] themes = { "valo", "reindeer", "runo", "chameleon" };
		ComboBox themePicker = new ComboBox("Theme", Arrays.asList(themes));
		themePicker.setNullSelectionAllowed(false);
		themePicker.setValue(vdi.getTheme());

		themePicker.addValueChangeListener(event -> {
			String theme = (String) event.getProperty().getValue();
			vdi.setTheme(theme);
		});
		addComponent(themePicker);
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

	}
}
