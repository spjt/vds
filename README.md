# vds
Vaadin reflective view dispatch

Vaadin is oriented towards Single Page Applications. If you already have an existing large web application, this can make it difficult to transition over. This servlet allows views to be accessed directly via a URL. Some small example views are included. So if the artifact is deployed to http://localhost/vds, they can be accessed with

`http://localhost/vds/dispatch` A simple "hello world" type view
`http://localhost/vds/themes` See different themes
`http://localhost/vds/google` An example that redirects to another page

